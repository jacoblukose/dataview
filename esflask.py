
from flask import render_template,request,Flask,json
from outis import elastopy
from werkzeug import secure_filename
from werkzeug.wsgi import LimitedStream
from datetime import datetime, timedelta
import csv
import os

class StreamConsumingMiddleware(object):

	def __init__(self, app):
		self.app = app

	def __call__(self, environ, start_response):
		stream = LimitedStream(environ['wsgi.input'],
							   int(environ['CONTENT_LENGTH'] or 0))
		environ['wsgi.input'] = stream
		app_iter = self.app(environ, start_response)
		try:
			stream.exhaust()
			for event in app_iter:
				yield event
		finally:
			if hasattr(app_iter, 'close'):
				app_iter.close()

app = Flask(__name__, static_folder='static')
UPLOAD_FOLDER = '/home/jake/test1/profoundis/uploads'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER 

c=elastopy()

@app.route('/')
def dashboard():
	l=c.map_key('sanjana')
	l=l.keys()
	l.sort()
	print l
	return render_template('/home.html',keys=l,indexes=c.get_indices())


@app.route('/api/v1/data/search',methods=['GET','POST'])
def search():
	day_tme = datetime.now()
	yest = day_tme + timedelta(days =365)
	yest_iso = yest.isoformat()		
	l=datetime.strptime(yest_iso,'%Y-%m-%dT%H:%M:%S.%f') 

	l=c.map_key('sanjana')
	l=l.keys()
	l.sort()
	l2=[]
	if request.method == 'POST':
		data=request.get_json()
		t=[]
		temp=[]
		print data
		for items in l:
			try:
				if data[items] == True:
					l2.append(items)
			except:
				pass

		# print data['option']
		try:
			val=c.search(data['query'],['something'],data['option'],c.get_indices())
			for i in val['hits']['hits']:
				time=i['_source']['time']
				p=datetime.strptime(time,'%Y-%m-%dT%H:%M:%S.%f+05:30')
				i['_source']['time']=p.strftime('%A, %B %d, %Y at %H:%M hours') 
				
				t.append(i)
			if t==[]:
				t=["No entries found"]
			
		except:
			t=["No entries found "]

		
		l=c.map_key('sanjana')
		l=l.keys()
		l=['attempt','url']
		
		with open('static/data.csv', 'wb') as output_file:
			dict_writer = csv.DictWriter(output_file, l2,extrasaction='ignore')
			dict_writer.writeheader()
			try:
				for k in t:
					dict_writer.writerows([k['_source']])
			except:
				print "SDF"

		for idx,value in enumerate(val['aggregations']['2']['buckets']):
			time=val['aggregations']['2']['buckets'][idx]['key_as_string']
			p=datetime.strptime(time,'%Y-%m-%dT%H:%M:%S.%f+05:30')
			val['aggregations']['2']['buckets'][idx]['key_as_string']=p.strftime(' %B %d, %Y')
		
		t.append(val['aggregations']['2']['buckets'])
		# print json.dumps(t)
		
		return json.dumps(t)
	

@app.route('/api/v1/data/estimate',methods=['POST'])
def aggregate():
	if request.method == 'POST':
		data=request.get_json()
		l=c.aggregate(data['option'],data['query'],'')
		if data['option'] == 'terms':
			print json.dumps(l['sample_name']['buckets'])
			return json.dumps(l['sample_name']['buckets'])
		else:
			print json.dumps(l)
			return json.dumps(l['sample_name']['value'])
@app.route('/api/v1/data/upload',methods=['POST'])
def upload():
	if request.method =='POST':
		file = request.files['file']
		if file:
			filename = secure_filename(file.filename)
			file.save(os.path.join(app.config['UPLOAD_FOLDER'],filename))
			c.upload(filename,'sanjana')
			return 'success'
	


if __name__ == "__main__":
	app.debug = True
	app.run()
	app.wsgi_app = StreamConsumingMiddleware(app.wsgi_app)
