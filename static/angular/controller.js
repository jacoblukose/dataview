angular.module('es-app')
    .controller('searchCtrl', searchCtrl);


/* Inject Dependencies */
searchCtrl.$inject = [
    '$scope', 'searchService'
];



function searchCtrl($scope, searchService){
	$scope.vale = null;
	$scope.search={};
	$scope.aggr={};
	$scope.val=90;
	$scope.array = [{ "value":   "5y", "text": "Before 5 years" }
				    , { "value": "2y", "text": "Before 2 years" }
				    , { "value": "1y", "text": "Before 1 years" }
				    , { "value": "6M", "text": "Before 6 months" }
				    , { "value": "1M", "text": "Before 1 months" }
				    , { "value": "1s", "text": "Till present" }];

	// $scope.getkeys=getkeys;


	$scope.searchdata=searchdata;
	$scope.aggrdata=aggrdata;
	$scope.uploadFile=uploadFile;
	$scope.reset=reset;
	$scope.resetaggr=resetaggr;

		$(document).ready(function(){
    $('#myTable').DataTable();
		});
	
	// $scope.hello=hello;

	// getkeys();
	// function getkeys(data){
	// 	searchService.getkey_fn(data)
	// 	.then(function(data)
	// 	{
	// 		$scope.keyvalues=data.data;
	// 		console.log("testing");
	// 	});
	// }
	// hello();

	function searchdata(data){

		searchService.search_fn(data)
		.then(function(data)
		{
			key_list=[];
			doc_list=[];

			$scope.temp=data.data;
			hello=data.data;
			$scope.last=hello[hello.length - 1 ];
		
		
		
   
	for (x in $scope.last) 
			{
    			txt = JSON.stringify($scope.last[x].key_as_string);
    			console.log(txt)
    			txt1 = JSON.stringify($scope.last[x].doc_count);
    			key_list.push(txt1);
    			doc_list.push(txt);
			}



			
			

				console.log(key_list);
var ctx = document.getElementById("myChart");
var randomColorGenerator = function () { 
    return '#' + (Math.random().toString(16) + '0000000').slice(2, 8); 
};
var myChart = new Chart(ctx, {
    type: 'bar',
    data :{
    labels:  doc_list ,
    datasets: [
        { 
            label: "Document count ",
            fill: true,
            lineTension: 0.1,
            backgroundColor: randomColorGenerator(),
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            
            pointRadius: 1,
            pointHitRadius: 10,
            data: key_list,
        }
    ]
}

    
});	
		});
	}

	function reset(){

			$scope.temp={};
			doc_list=[];
			key_list=[];
			console.log("testing in reset of searchdata");
		
	}

	function resetaggr(){

			$scope.temp2={};
			console.log("testing in reset of aggregations");
		
	}

	function aggrdata(data){

		searchService.aggr_fn(data)
		.then(function(data)
		{
			doc_list=[];
			key_list=[];
			
			$scope.temp2=data.data;
			var txt="";
			var txt1="";
			var x;
			
			for (x in $scope.temp2) 
			{
    			txt = JSON.stringify($scope.temp2[x].doc_count);
    			txt1 = JSON.stringify($scope.temp2[x].key);
    			key_list.push(txt);
    			doc_list.push(txt1);
			}
			
			

				console.log(key_list);
var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
    type: 'bar',
    data :{
    labels:  doc_list ,
    datasets: [
        { 
           
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: key_list,
        }
    ]
}

    
});
			console.log(key_list);
			console.log(doc_list);
		});

	}

function uploadFile(data){

	var file = $scope.myFile;        
    console.log('file is ' );
    console.dir(file);

	searchService.upload_fn(data)
	.then(function(data)
	{    
       console.log("inside uplaod fucniton")
	});
	


// function hello ($scope) {

//   $scope.labels = ["January", "February", "March", "April", "May", "June", "July"];
//   $scope.series = ['Series A', 'Series B'];
//   $scope.data = [
//     [65, 59, 80, 81, 56, 55, 40],
//     [28, 48, 40, 19, 86, 27, 90]
//   ];
//   $scope.onClick = function (points, evt) {
//     console.log(points, evt);
//   };
}



    
// });


}